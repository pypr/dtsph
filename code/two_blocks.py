from pysph.examples.two_blocks import TwoBlocks
from dtsph import DTSPHScheme


dx = 0.025
hdx = 1.0
rho0 = 1000
u0 = 1.0
c0 = 10*u0


class TwoBlocksDTSPH(TwoBlocks):
    def create_scheme(self):
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=0.0, dtaufac=0.1,
            cs=c0, vref=u0, rho=rho0, alpha=0.15, debug=False
        )
        return dtsph

    def configure_scheme(self):
        dt = 2e-3
        tf = 1.0
        self.scheme.configure_solver(
                dt=dt, tf=tf, pfreq=1
        )


if __name__ == '__main__':
    app = TwoBlocksDTSPH()
    app.run()
