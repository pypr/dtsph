
# Note that this example is best run with --cache-nnps.

from pysph.examples import cavity

from dtsph import DTSPHSteadyScheme

c0 = cavity.c0
Umax = cavity.Umax


class LDC(cavity.LidDrivenCavity):
    def consume_user_options(self):
        super().consume_user_options()
        self.options.scheme = 'dtsph'

    def create_scheme(self):
        s = DTSPHSteadyScheme(['fluid'], ['solid'], dim=2, nu=0.0, cs=c0, h=None)
        return s

    def configure_scheme(self):
        h0 = self.dx * cavity.hdx
        tf = 10.0
        dt_cfl = 0.25 * h0 / (c0 + Umax)
        dt_viscous = 0.125 * h0**2 / self.nu
        dt = min(dt_cfl, dt_viscous)
        self.scheme.configure(nu=self.nu, h=h0)
        self.scheme.configure_solver(tf=tf, dt=dt, pfreq=500)


if __name__ == '__main__':
    app = LDC()
    app.run()
    app.post_process(app.info_filename)
