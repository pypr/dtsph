"""Linear advection problem.
"""

import os
import numpy as np

from pysph.base.kernels import QuinticSpline
from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.solver.utils import iter_output
from pysph.sph.scheme import WCSPHScheme, SchemeChooser
from pysph.sph.wc.edac import EDACScheme

from dtsph import DTSPHScheme

layers = 6
L = 1

class PressureBlip(Application):
    def initialize(self):
        self.rho0 = 1.0
        self.L = L
        self.u = u = 1.0
        self.v = v = 1.0
        self.U = np.sqrt(u*u + v*v)
        self.c0 = 10
        self.layers = layers
        self.hdx = 1.3
        self.dx = 0.02

    def add_user_options(self, group):
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        group.add_argument(
            "--perturb", action="store", type=float, dest="perturb", default=0,
            help="Random perturbation of initial particles as a fraction "
            "of dx (setting it to zero disables it, the default)."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.0,
            help="Ratio h/dx."
        )

    def create_domain(self):
        return DomainManager(
            xmin=-self.L, xmax=self.L, ymin=-self.L, ymax=self.L,
            periodic_in_x=True, periodic_in_y=True, n_layers=3.0
        )

    def consume_user_options(self):
        nx = self.options.nx
        self.dx = dx = self.L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        self.h0 = self.hdx * self.dx
        cfl = self.options.cfl_factor
        self.dt = cfl * self.h0 / (self.U+self.c0) * 5

        self.tf = 2.0

    def create_particles(self):
        # create the particles
        dx = self.dx
        _x = np.arange(-self.L+dx/2, self.L, dx)
        x, y = np.meshgrid(_x, _x)

        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        m = self.volume * self.rho0
        h = self.hdx * dx
        p = np.ones_like(x) * (-1)
        p[x**2 + y**2 < 0.34**2] = 10

        fluid = get_particle_array(
            name='fluid', x=x, y=y, m=m, h=h, rho=self.rho0, p=p
        )
        self.scheme.setup_properties([fluid])
        return [fluid]

    def configure_scheme(self):
        scheme = self.scheme
        kernel = QuinticSpline(dim=2)
        tf = 3.0
        dt = 0.25*self.hdx*self.dx/(self.U + self.c0)
        if self.options.scheme == 'wcsph':
            scheme.configure(h0=self.hdx*self.dx)
            scheme.configure_solver(
                kernel=kernel, dt=dt, tf=tf,
                adaptive_timestep=False,
                output_at_times=[0.05, 0.5, 1.0]
            )
        elif self.options.scheme == 'edac':
            scheme.configure_solver(
                kernel=kernel,
                dt=dt, tf=tf,
                adaptive_timestep=False,
                output_at_times=[0.05, 0.5, 1.0]
            )
        elif self.options.scheme in ['dtsph']:
            dt *= 10
            scheme.configure_solver(
                kernel=kernel, dt=dt, tf=tf,
                adaptive_timestep=False,
                output_at_times=[0.05, 0.5, 1.0]
            )

    def create_scheme(self):
        h0 = self.hdx*self.dx
        wcsph = WCSPHScheme(
            ['fluid'], [], dim=2, rho0=self.rho0, c0=self.c0,
            h0=h0, hdx=self.hdx, gamma=7.0, alpha=0.15, beta=0.0
        )
        edac = EDACScheme(
            ['fluid'], [], dim=2, rho0=self.rho0, c0=self.c0, pb=0.0,
            h=h0, nu=0.0, eps=0.0
        )
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=0.001, dtaufac=0.1,
            cs=self.c0, vref=self.U, rho=self.rho0, alpha=0.15, debug=False,
            h=h0
        )
        s = SchemeChooser(default='dtsph', wcsph=wcsph, edac=edac, dtsph=dtsph)
        return s

    def _make_final_plot(self):
        try:
            import matplotlib
            matplotlib.use('Agg')
            from matplotlib import pyplot as plt
        except ImportError:
            print("Post processing requires matplotlib.")
            return
        last_output = self.output_files[-1]
        from pysph.solver.utils import load
        data = load(last_output)
        pa = data['arrays']['fluid']
        tf = data['solver_data']['t']
        plt.scatter(pa.x, pa.y, marker='.')
        plt.ylim(-2, 2)
        plt.xlim(plt.ylim())
        plt.title("Particles at %s secs" % tf)
        plt.xlabel('x')
        plt.ylabel('y')
        fig = os.path.join(self.output_dir, "final.png")
        plt.savefig(fig, dpi=300)
        print("Figure written to %s." % fig)

    def post_process(self, info_file_or_dir):
        if self.rank > 0:
            return
        self.read_info(info_file_or_dir)
        if len(self.output_files) == 0:
            return
        self._make_final_plot()


if __name__ == '__main__':
    app = PressureBlip()
    app.run()
