#!/usr/bin/env python
import os
import shutil
import json
import glob
from itertools import cycle, product

from automan.api import PySPHProblem as Problem
from automan.api import (Automator, Simulation, filter_by_name,
                         compare_runs, filter_cases)
import numpy as np
import matplotlib
from pysph.solver.utils import load
matplotlib.use('agg')


n_core = 4
n_thread = 4
backend = ' --openmp'


def _get_cpu_time(case):
    info = glob.glob(case.input_path('*.info'))[0]
    with open(info) as fp:
        data = json.load(fp)
    return round(data['cpu_time'], 2)


def _save_perf(out_dir, timings):
    path = os.path.join(out_dir, 'performance.json')
    data = {}
    if os.path.exists(path):
        with open(path, 'r+') as f:
            data = json.load(f)
    data.update(timings)
    with open(path, 'w') as f:
        json.dump(data, f, indent=4)


def make_table(column_names, row_data, output_fname, sort_cols=None,
               **extra_kw):
    import pandas as pd
    col_data = [[] for i in range(len(column_names))]
    for row in row_data:
        for i, item in enumerate(row):
            col_data[i].append(item)
    d = {n: col_data[i] for i, n in enumerate(column_names)}
    df = pd.DataFrame(d, columns=column_names)
    if sort_cols:
        sort_by = [column_names[i] for i in sort_cols]
    else:
        sort_by = column_names[-1]
    df.sort_values(by=sort_by, inplace=True)
    parent = os.path.dirname(output_fname)
    if not os.path.exists(parent):
        os.mkdir(parent)
    with open(output_fname, 'w') as fp:
        fp.write(df.to_latex(index=False, escape=False, **extra_kw))


def get_files_at_given_times(files, times):
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if count >= len(times):
            break
        if abs(t - times[count]) < t*1e-8:
            result.append(f)
            count += 1
        elif t > times[count]:
            count += 1
    return result


def merge_records(case_info):
    for key in case_info:
        info = case_info[key]
        if isinstance(info, tuple):
            if len(info) == 2:
                info, extra = case_info[key]
                info.update(extra)
            else:
                info = info[0]
            case_info[key] = info


def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    return params


class SquarePatch(Problem):
    def get_name(self):
        return 'square_patch'

    def setup(self):
        get_path = self.input_path
        nx = [50, 100, 200]
        cmd = 'python code/square_patch.py' + backend
        kernel = 'QuinticSpline'
        alpha = 0.1
        tol = 1e-3

        self.case_info = {
            f'edac_nx_{i}': dict(
                scheme='edac', alpha=alpha, no_adaptive_timestep=None,
                nx=i
            ) for i in nx
        }
        self.case_info.update({
            f'dtsph_edac_tol_0.001_nx_{i}': dict(
                scheme='dtsph', tol=1e-3, alpha=alpha, edac=None, shift=None,
                nx=i
            ) for i in nx
        })

        self.case_pdata = {
            f'edac_nx_{i}': dict(
                tol='NA', label=f'EDAC, nx={i}', to_plot=False,
                measure_perf=False
            ) for i in nx
        }
        self.case_pdata.update({
            f'dtsph_edac_tol_0.001_nx_{i}': dict(
                label=f'DTSPH, nx={i}', to_plot=True, tol=tol,
                measure_perf=False
            ) for i in nx
        })

        for s in self.case_info:
            if (self.case_info[s]['nx'] in [200] and 'edac' in s
                and 'dtsph' not in s):
                self.case_pdata[s].update({'to_plot': True})
            if (self.case_info[s]['nx'] in [100] and 'dtsph' in s):
                self.case_pdata[s].update({'to_plot': False})

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._make_table()
        self._particle_plots()

    def _make_table(self):
        column_names = ['Scheme', r'nx', r'$\epsilon$', 'CPU time (secs)']
        rows = []
        for s in self.case_info:
            tol = str(self.case_pdata[s]['tol'])
            nx = self.case_info[s]['nx']
            time = _get_cpu_time(self.case_info[s]['case'])
            label = self.case_pdata[s]['label']
            row = [label, nx, tol, time]
            rows.append(row)

        tmp_perf = {}
        for s in self.case_info:
            time = _get_cpu_time(self.case_info[s]['case'])
            label = self.case_pdata[s]['label']
            measure_perf = self.case_pdata[s]['measure_perf']
            tmp_perf.update({s: dict(label=label, time=time,
                                     measure_perf=measure_perf)})
        performance = {self.get_name(): tmp_perf}
        _save_perf(self.out_dir, performance)

        make_table(
            column_names, rows, self.output_path(self.get_name() + '_time.tex'),
            sort_cols=[1, -1], column_format='lrrr'
        )

    def _particle_plots(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files
        import string

        ids = cycle(string.ascii_lowercase)

        plot_data = {}
        vmin, vmax = 0, 0
        for name in self.case_info:
            if self.case_pdata[name]['to_plot']:
                files = get_files(self.input_path(name), 'square_patch')
                data = load(files[-1])
                pa = data['arrays']['fluid']
                tf = data['solver_data']['t']
                vmag = np.sqrt(pa.u**2 + pa.v**2)
                vmin = min(vmin, min(vmag))
                vmax = max(vmax, max(vmag))
                plot_data[name] = dict(x=pa.x.copy(), y=pa.y.copy(),
                                       vmag=vmag.copy(), p=pa.p.copy(), t=tf)
        figlen = len(plot_data)
        fig, ax = plt.subplots(1, figlen, figsize=(5*figlen, 4),
                               sharex=True, sharey=True)
        fig.subplots_adjust(hspace=0)
        ax = ax.ravel()
        i = 0
        for name, data in plot_data.items():
            tmp = ax[i].scatter(
                data['x'], data['y'], marker='.', c=data['vmag'],
                edgecolors='none', linewidth=0, cmap='jet',
                rasterized=True, vmin=vmin, vmax=vmax
            )
            fig.colorbar(tmp, ax=ax[i-1], shrink=0.95, label='vmag')
            ax[i].set_ylim(-2, 2)
            ax[i].set_xlabel(
                f'({next(ids)}) '+self.case_pdata[name]['label'],
                fontsize=12
            )
            i += 1
        fig.savefig(self.output_path('square_patch.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class EllipticalDrop(Problem):
    def get_name(self):
        return 'elliptical_drop'

    def setup(self):
        get_path = self.input_path
        nx = 200
        alpha = 0.15
        cmd = 'pysph run elliptical_drop' + backend
        dtsph_cmd = 'python code/elliptical_drop.py' + backend
        kernel = 'QuinticSpline'
        tol = 1e-4

        self.case_info = {
            'deltasph': dict(
                scheme='wcsph', base_command=cmd, no_adaptive_timestep=None,
                kernel=kernel, delta_sph=None, alpha=alpha, nx=200
            ),
            'edac': dict(
                base_command=dtsph_cmd, scheme='edac',
                kernel=kernel, alpha=alpha, nx=200
            ),
        }
        nxs = [50, 100, 200]
        self.case_info.update({
            f'dtsph_edac_nx_{n}': dict(
                base_command=dtsph_cmd, scheme='dtsph', tol=tol,
                kernel=kernel, alpha=alpha, edac=None, shift=None, nx=n
            ) for n in nxs
        })

        self.case_pdata = {
            'deltasph': dict(
                label=fr'$\delta$-SPH, nx={nx}', to_plot=True, measure_perf=False,
                to_particle_plot=False
            ),
            'edac': dict(
                label=f'EDAC, nx={nx}', to_plot=True, measure_perf=False,
                to_particle_plot=True
            ),
        }
        self.case_pdata.update({
            f'dtsph_edac_nx_{nx}': dict(
                label=fr'DTSPH, nx={nx}', to_plot=True, measure_perf=True,
                to_particle_plot=True
            )
            for nx in nxs
        })

        for s in self.case_info:
            if 'deltasph' in s:
                self.case_pdata[s].update({'to_particle_plot': False})
            if self.case_info[s]['nx'] in [100] and 'dtsph' in s:
                self.case_pdata[s].update({'to_particle_plot': False})

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(scheme)
            ) for name, scheme in self.case_info.items()
        ]
        merge_records(self.case_info)

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_axis_and_ke()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        for ci in self.case_info.values():
            row = [ci['scheme'].upper(), _get_cpu_time(ci['case'])]
            rows.append(row)

        tmp_perf = {}
        for s in self.case_info:
            time = _get_cpu_time(self.case_info[s]['case'])
            label = self.case_pdata[s]['label']
            measure_perf = self.case_pdata[s]['measure_perf']
            tmp_perf.update({s: dict(label=label, time=time,
                                     measure_perf=measure_perf)})
        performance = {self.get_name(): tmp_perf}
        _save_perf(self.out_dir, performance)

        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            column_format='lr'
        )

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files
        from pysph.examples.elliptical_drop import exact_solution
        import string

        ids = cycle(string.ascii_lowercase)

        plot_data = {}
        vmin, vmax = 0, 0
        for name in self.case_info:
            if self.case_pdata[name]['to_particle_plot']:
                files = get_files(self.input_path(name), 'elliptical_drop')
                data = load(files[-1])
                pa = data['arrays']['fluid']
                tf = data['solver_data']['t']
                vmag = np.sqrt(pa.u**2 + pa.v**2)
                vmin = min(vmin, min(pa.p))
                vmax = max(vmax, max(pa.p))
                plot_data[name] = dict(x=pa.x.copy(), y=pa.y.copy(),
                                       vmag=vmag.copy(), p=pa.p.copy(), t=tf)
        figlen = len(plot_data)
        fig, ax = plt.subplots(1, figlen, figsize=(5*figlen, 8),
                               sharex=True, sharey=True)
        ax = ax.ravel()
        i = 0
        for name, data in plot_data.items():
            _a, _A, _po, xe, ye = exact_solution(data['t'])
            ax[i].plot(xe, ye)
            tmp = ax[i].scatter(
                data['x'], data['y'], marker='.', c=data['p'],
                edgecolors='none', linewidth=0, cmap='jet',
                rasterized=True, vmin=vmin, vmax=vmax
            )
            fig.colorbar(tmp, ax=ax[i-1], shrink=0.95, label='p')
            ax[i].set_ylim(-2, 2)
            ax[i].set_xlabel(
                f'({next(ids)}) '+self.case_pdata[name]['label'],
                fontsize=12
            )
            i += 1
        fig.savefig(self.output_path('elliptical_drop_p.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

    def _plot_axis_and_ke(self):
        import matplotlib.pyplot as plt

        data = {}
        linestyles = cycle(['k--', 'k:', 'k-', 'k-.', 'k-*'])
        for scheme in self.case_info:
            data[scheme] = np.load(self.input_path(scheme, 'results.npz'))

        plt.figure(figsize=(6, 5))
        for scheme in self.case_info:
            if self.case_pdata[scheme]['to_plot']:
                plt.plot(
                    data[scheme]['t'],
                    np.abs(data[scheme]['ymax'] - data[scheme]['major']),
                    next(linestyles), label=self.case_pdata[scheme]['label']
                )
        plt.xlabel(r't')
        plt.ylabel('Error in major-axis')
        plt.legend(loc='upper left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('major_axis.pdf'))
        plt.clf()
        plt.close()

        plt.figure(figsize=(6, 5))
        for scheme in self.case_info:
            if self.case_pdata[scheme]['to_plot']:
                plt.plot(
                    data[scheme]['t'],
                    np.abs(data[scheme]['ke'] - data[scheme]['major']),
                    next(linestyles), label=self.case_pdata[scheme]['label']
                )
        plt.xlabel(r't')
        plt.ylabel('Kinetic Energy')
        plt.legend(loc='lower left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('ke.pdf'))
        plt.close()


class DamBreak2D(Problem):
    def get_name(self):
        return 'dam_break_2d'

    def setup(self):
        get_path = self.input_path

        tf = 1.0
        alpha = 0.1
        kernel = 'QuinticSpline'
        tol = 1e-4

        cmd = 'pysph run dam_break_2d' + backend
        dtsph_cmd = 'python code/dam_break_2d.py' + backend

        _case_info = {
            'dtsph_edac': (
                dict(
                    base_command=dtsph_cmd, alpha=alpha, tol=tol,
                    pfreq=100, kernel=kernel, edac=None, shift=None
                ),
                dict(to_plot=True, label='DTSPH', measure_perf=True)
            ),
            'deltasph': (
                dict(
                    base_command=cmd, scheme='wcsph', no_adaptive_timestep=None,
                    alpha=alpha, kernel=kernel, delta_sph=None
                ),
                dict(to_plot=False, label=r'$\delta$-SPH', measure_perf=True)
            ),
            'edac': (
                dict(
                    base_command=dtsph_cmd, scheme='edac',
                    no_adaptive_timestep=None, alpha=alpha, kernel=kernel
                ),
                dict(to_plot=True, label=r'EDAC', measure_perf=True)
            )
        }

        self.case_info = {
            f'{s}': dict(
                tf=tf, **_case_info[s][0]
            )
            for s in _case_info
        }

        self.case_pdata = {
            f'{s}': _case_info[s][1]
            for s in _case_info
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme
            ) for name, scheme in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_toe()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        for s in self.case_info:
            row = [self.case_pdata[s]['label'],
                   _get_cpu_time(self.case_info[s]['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            column_format='lr'
        )

        tmp_perf = {}
        for s in self.case_info:
            time = _get_cpu_time(self.case_info[s]['case'])
            label = self.case_pdata[s]['label']
            measure_perf = self.case_pdata[s]['measure_perf']
            tmp_perf.update({s: dict(label=label, time=time,
                                     measure_perf=measure_perf)})
        performance = {self.get_name(): tmp_perf}
        _save_perf(self.out_dir, performance)

    def _plot_toe(self):
        import matplotlib.pyplot as plt
        from pysph.examples import db_exp_data as dbd

        lstyle = cycle(['-', '--', ':', '-.'])

        data = {}
        plt.figure(figsize=(5, 4))
        for scheme in self.case_info:
            data[scheme] = np.load(self.input_path(scheme, 'results.npz'))
            plt.plot(
                data[scheme]['t'], data[scheme]['x_max'],
                label=self.case_pdata[scheme]['label'], linestyle=next(lstyle)
            )
            tmps, xmps = dbd.get_koshizuka_oka_mps_data()
        plt.plot(tmps, xmps, '^', label='MPS Koshizuka & Oka (1996)')
        factor = np.sqrt(2.0*9.81/1.0)
        plt.xlim(0, 0.7*factor)
        plt.ylim(0.5, 4.5)
        plt.xlabel(r'$T$')
        plt.ylabel(r'$Z/L$')
        plt.legend(loc='upper left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('toe_vs_t.pdf'))
        plt.close()

    def _plot_particles(self):
        from pysph.solver.utils import get_files

        times = [0.4, 0.6, 0.8]
        plot_data = {}
        for name in self.case_info:
            if self.case_pdata[name]['to_plot']:
                all_files = get_files(self.input_path(name), 'dam_break_2d')
                files = get_files_at_given_times(all_files, times)
                tmp = {}
                for i, f in enumerate(files):
                    print(i, f)
                    data = load(f)
                    pa = data['arrays']['fluid']
                    bo = data['arrays']['boundary']
                    vmag = np.sqrt(pa.u**2 + pa.v**2)
                    tmp[times[i]] = dict(
                        x=pa.x.copy(), y=pa.y.copy(), vmag=vmag, p=pa.p.copy(),
                        xb=bo.x.copy(), yb=bo.y.copy(), mb=bo.m.copy()
                    )
                plot_data[name] = tmp
        for scalar in ['p', 'vmag']:
            self._plot_util(plot_data, scalar, times)

    def _plot_util(self, plot_data, scalar, times):
        import matplotlib.pyplot as plt
        import string

        ids = cycle(string.ascii_lowercase)

        figlen = len(plot_data)
        fig, ax = plt.subplots(3, figlen, figsize=(4*figlen, 10), dpi=300,
                               sharex=True, sharey=True)
        for i, t in enumerate(times):
            vmin, vmax = 0, 0
            for name in plot_data:
                vmin = min(vmin, min(plot_data[name][t][scalar]))
                vmax = max(vmax, max(plot_data[name][t][scalar]))

            for j, name in enumerate(plot_data):
                data = plot_data[name][t]
                tmp = ax[i][j].scatter(
                    data['x'], data['y'], c=data[scalar], marker='.', s=6,
                    edgecolors='none', cmap='jet', linewidth=0,
                    rasterized=True, vmin=vmin, vmax=vmax
                )
                fig.colorbar(tmp, ax=ax[i][j], shrink=0.95, label=scalar,
                             format='%.0e' if scalar == 'p' else '%.1f')
                ax[i][j].scatter(
                    data['xb'], data['yb'], c=data['mb'], marker='.', s=6,
                    edgecolors='none', linewidth=0, rasterized=True
                )
                ax[i][j].annotate(
                    't = %.1f' % t, xy=(0.5, 3.5), fontsize=18
                )
                plt.xlim(-0.1, 4.1)
                plt.ylim(-0.1, 4.1)
                if i == len(times)-1:
                    ax[i][j].set_xlabel(
                        f'({next(ids)}) '+self.case_pdata[name]['label'],
                        fontsize=12
                    )
        fig.savefig(self.output_path(f'db2d_{scalar}.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class DamBreak3D(Problem):
    def get_name(self):
        return 'dam_break_3d'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/dam_break_3d.py ' + backend
        pysph_cmd = 'pysph run dam_break_3d' + backend
        alpha = 0.1
        tf = 1.0
        kernel = 'QuinticSpline'

        self.case_info = {
            'dtsph_edac_tol_1e-3': dict(
                base_command=cmd, tf=tf, pfreq=10, alpha=alpha, edac=None,
                scheme='dtsph', tol=1e-3
            ),
            'dtsph_edac_tol_1e-4': dict(
                base_command=cmd, tf=tf, pfreq=10, tol=1e-4, alpha=alpha,
                edac=None, scheme='dtsph'
            ),
            'edac': dict(
                base_command=cmd, tf=tf, scheme='edac',
                no_adaptive_timestep=None, pfreq=100,
                alpha=alpha
            ),
        }

        self.case_pdata = {
            'dtsph_edac_tol_1e-3': dict(
                label='DTSPH', tol=1e-3,
                to_plot=False, measure_perf=True
            ),
            'dtsph_edac_tol_1e-4': dict(
                label='DTSPH', tol=1e-4,
                to_plot=True, measure_perf=True
            ),
            'edac': dict(
                label=r'EDAC', tol='NA',
                to_plot=True, measure_perf=True
            ),
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                kernel=kernel, cache_nnps=None,
                **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', r'$\epsilon$', 'CPU time (secs)']
        rows = []
        for s in self.case_info:
            tol = str(self.case_pdata[s]['tol'])
            row = [self.case_pdata[s]['label'], tol,
                   _get_cpu_time(self.case_info[s]['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            column_format='lrr'
        )

        tmp_perf = {}
        for s in self.case_info:
            time = _get_cpu_time(self.case_info[s]['case'])
            label = self.case_pdata[s]['label']
            measure_perf = self.case_pdata[s]['measure_perf']
            tmp_perf.update({s: dict(label=label, time=time,
                                     measure_perf=measure_perf)})
        performance = {self.get_name(): tmp_perf}
        _save_perf(self.out_dir, performance)

    def _plot_particles(self):
        from pysph.solver.utils import get_files
        from mayavi import mlab
        mlab.options.offscreen = True

        for name in self.case_info:
            if self.case_pdata[name]['to_plot']:
                fname = 'dam_break_3d'
                files = get_files(self.input_path(name), fname)
                times = [0.4, 0.6, 1.0]
                to_plot = get_files_at_given_times(files, times)
                mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(800, 800))
                for i, f in enumerate(to_plot):
                    print(i, f)
                    data = load(f)
                    t = data['solver_data']['t']
                    f = data['arrays']['fluid']
                    b = data['arrays']['boundary']
                    o = data['arrays']['obstacle']
                    bg = mlab.points3d(
                        b.x, b.y, b.z, mode='point',
                        opacity=0.1, color=(0, 0, 0)
                    )
                    bg.actor.property.point_size = 2.5
                    fg = mlab.points3d(
                        f.x, f.y, f.z, f.u, mode='point',
                        colormap='viridis', vmin=0, vmax=1
                    )
                    fg.actor.property.render_points_as_spheres = True
                    fg.actor.property.point_size = 5

                    mlab.scalarbar(fg, title='u', orientation='horizontal')

                    og = mlab.points3d(
                        o.x, o.y, o.z, mode='point',
                        color=(0, 0, 1)
                    )
                    og.actor.property.render_points_as_spheres = True
                    og.actor.property.point_size = 8
                    opath = self.output_path(name + f'_u_{i}.png')
                    mlab.savefig(opath)
                    mlab.clf()


class Cavity(Problem):
    def get_name(self):
        return 'cavity'

    def setup(self):
        get_path = self.input_path
        self.nx = [50, 100, 150]
        tf = 10
        self.re = re = [100]

        cmd = 'pysph run cavity ' + backend
        dtsph_cmd = 'python code/cavity.py ' + backend
        _case_info = {
            'dtsph_edac': (
                dict(base_command=dtsph_cmd, pfreq=100, edac=None, shift=None),
                dict(label='DTSPH', to_plot=True, measure_perf=True)
            ),
            'tvf': (
                dict(base_command=cmd, scheme='tvf', no_adaptive_timestep=None),
                dict(label='TVF', to_plot=True, measure_perf=True)
            )
        }

        self.case_info = {
            f'{s}_nx_{n}_re_{r}': dict(
                nx=n, re=r, tf=tf, **_case_info[s][0]
            )
            for s, n, r in product(_case_info, self.nx, re)
        }

        self.case_pdata = {
            f'{s}_nx_{n}_re_{r}': dict(
                label=_case_info[s][1]['label'] + f', nx={n}',
                to_plot=_case_info[s][1]['to_plot'],
                measure_perf=_case_info[s][1]['measure_perf']
            )
            for s, n, r in product(_case_info, self.nx, re)
        }

        for s in self.case_info:
            if s.endswith('1000'):
                self.case_info[s].update({'tf': 50})
            if self.case_info[s]['nx'] == 50:
                self.case_pdata[s].update({'measure_perf': False})
            if self.case_info[s]['nx'] in [50, 100] and 'tvf' in s:
                self.case_pdata[s].update({'to_plot': False})

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **kwargs
            ) for name, kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', 'nx', 'Re', 'CPU time (secs)']
        rows = []
        for name in self.case_info:
            row = [self.case_pdata[name]['label'], self.case_info[name]['nx'],
                   self.case_info[name]['re'],
                   _get_cpu_time(self.case_info[name]['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            sort_cols=[1, 2, -1], column_format='lrrr'
        )

        tmp_perf = {}
        for s in self.case_info:
            time = _get_cpu_time(self.case_info[s]['case'])
            label = self.case_pdata[s]['label']
            measure_perf = self.case_pdata[s]['measure_perf']
            tmp_perf.update({s: dict(label=label, time=time,
                                     measure_perf=measure_perf)})
        performance = {self.get_name(): tmp_perf}
        _save_perf(self.out_dir, performance)

    def _plot_u_profile(self):
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig, ax = plt.subplots(2, 1, figsize=(7, 10))
            fig.subplots_adjust(hspace=0)
            ax[0].plot(exp_u[re], y, 'ko', fillstyle='none',
                        label=f'Ghia et al. (Re={re})')
            ax[0].set_xlabel('$u$')
            ax[0].set_ylabel('$y$')
            ax[1].plot(x, exp_v[re], 'ko', fillstyle='none',
                        label=f'Ghia et al. (Re={re})')
            ax[1].set_xlabel('$x$')
            ax[1].set_ylabel('$v$')
            linestyles = cycle(['k-', 'k--', 'k:', 'b-.', 'b-1', 'b-2'])

            for name in self.case_info:
                if (self.case_pdata[name]['to_plot'] and
                    self.case_info[name]['re'] == re):
                    data = np.load(self.input_path(name, 'results.npz'))
                    lstyle = next(linestyles)
                    ax[0].plot(data['u_c'], data['x'], lstyle,
                                label=self.case_pdata[name]['label'])
                    ax[1].plot(data['x'], data['v_c'], lstyle,
                                label=self.case_pdata[name]['label'])
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
            ax[0].set_aspect('equal', 'box')
            ax[1].set_aspect('equal', 'box')
        fig.savefig(self.output_path(f'uv_re{re}.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files
        from pysph.tools.interpolator import Interpolator

        for case in filter_cases(self.cases, re=100, nx=150):
            if 'dtsph' not in case.name:
                continue
            fig, ax = plt.subplots(1, 2, figsize=plt.figaspect(0.5))
            files = get_files(case.input_path(), 'cavity')
            data = load(files[-1])
            f = data['arrays']['fluid']
            vmag = np.sqrt(f.u**2 + f.v**2)
            ax[0].scatter(
                f.x, f.y, s=10, c=vmag, edgecolors='none',
                alpha=0.8, rasterized=True
            )
            _x = np.linspace(0, 1, 101)
            xx, yy = np.meshgrid(_x, _x)
            interp = Interpolator(list(data['arrays'].values()), x=xx, y=yy)
            ui = np.zeros_like(xx)
            vi = np.zeros_like(xx)
            interp.update_particle_arrays(list(data['arrays'].values()))
            _u = interp.interpolate('u')
            _v = interp.interpolate('v')
            _u.shape = 101, 101
            _v.shape = 101, 101
            ui += _u
            vi += _v
            ax[1].streamplot(xx, yy, ui, vi, linewidth=0.2, density=3,
                             color='k')
            for i in range(2):
                ax[i].set_xlim([0, 1])
                ax[i].set_ylim([0, 1])
                ax[i].set_xlabel('x')
                ax[i].set_ylabel('y')
            fig.tight_layout(pad=0)
            fig.savefig(self.output_path('streamplot.pdf'))
            plt.close()


class SteadyCavity(Cavity):
    def get_name(self):
        return 'steady_cavity'

    def _make_table(self):
        pass

    def setup(self):
        get_path = self.input_path
        self.nx = [50, 100, 150]
        tf = 10
        self.re = re = [100, 1000]
        kernel = 'QuinticSpline'

        cmd = 'python code/steady_cavity.py' + backend
        _case_info = {
            'dtsph': (
                dict(base_command=cmd),
                dict(to_plot=True),
            ),
            'dtsph_edac': (
                dict(base_command=cmd, edac=None),
                dict(to_plot=False)
            )
        }

        self.case_info = {
            f'{s}_nx_{n}_re_{r}': dict(
                nx=n, re=r, kernel=kernel, tf=tf, **_case_info[s][0]
            )
            for s, n, r in product(_case_info.keys(), self.nx, re)
        }

        self.case_pdata = {
            f'{s}_nx_{n}_re_{r}': dict(
                label=f'DTSPH, nx={n}',
                to_plot=_case_info[s][1]['to_plot'], measure_perf=False
            )
            for s, n, r in product(_case_info.keys(), self.nx, re)
        }

        for s in self.case_info:
            if s.endswith('1000'):
                self.case_info[s].update({'tf': 50})
        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **kwargs
            ) for name, kwargs in self.case_info.items()
        ]
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()
        self._make_table()

    def _plot_u_profile(self):
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig, ax = plt.subplots(2, 1, figsize=(7, 10))
            fig.subplots_adjust(hspace=0 if re == 100 else 0.2)
            ax[0].plot(exp_u[re], y, 'ko', fillstyle='none',
                       label=f'Ghia et al. (Re={re})')
            ax[0].set_xlabel('$u$')
            ax[0].set_ylabel('$y$')
            ax[1].plot(x, exp_v[re], 'ko', fillstyle='none',
                       label=f'Ghia et al. (Re={re})')
            ax[1].set_xlabel('$x$')
            ax[1].set_ylabel('$v$')
            linestyles = cycle(['k-', 'k--', 'r:', 'r-.', 'b-1', 'b-2'])

            for name in self.case_info:
                if self.case_pdata[name]['to_plot'] and \
                    self.case_info[name]['re'] == re:
                    data = np.load(self.input_path(name, 'results.npz'))
                    lstyle = next(linestyles)
                    ax[0].plot(data['u_c'], data['x'], lstyle,
                               label=self.case_pdata[name]['label'])
                    ax[1].plot(data['x'], data['v_c'], lstyle,
                               label=self.case_pdata[name]['label'])
            ax[0].legend(loc='best')
            ax[1].legend(loc='best')
            ax[0].set_aspect('equal', 'box')
            ax[1].set_aspect('equal', 'box')
            fig.savefig(self.output_path(f'uv_re{re}.pdf'),
                        bbox_inches='tight', pad_inches=0)
            plt.close()


class TGV(Simulation):
    def __init__(self, root, base_command, job_info=None, depends=None,
                 labels=None, **kw):
        self.labels = labels
        super(TGV, self).__init__(root, base_command, job_info, depends, **kw)

    def get_labels(self, labels):
        return self.labels

    def decay_exact(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        label = kw.pop('label', 'Exact')
        plt.semilogy(data['t'], data['decay_ex'], label=label, **kw)

    def decay(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.semilogy(data['t'], data['decay'], **kw)
        plt.xlabel('t')
        plt.ylabel('max velocity')

    def l1(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['l1'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error')

    def linf(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['linf'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_\infty$ error')

    def p_l1(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['p_l1'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error for $p$')


class TaylorGreen(Problem):
    def get_name(self):
        return "taylor_green"

    def setup(self):
        get_path = self.input_path
        kernel = 'QuinticSpline'
        re = 100
        perturb = 0.1
        tf = 2.5

        cmd = 'python code/taylor_green.py' + backend

        ########################################
        # Change tolerence
        ########################################
        dtaufac = 0.1
        tols = [1e-2, 1e-4, 1e-5]
        nx = 100
        dtsph_cs = 5

        self.case_info = {
            f'dtsph_edac_tol_{tol:.0e}': dict(
                nx=nx, tol=tol, tf=tf, kernel=kernel, re=re, dtaufac=dtaufac,
                perturb=perturb, base_command=cmd, dtsph_cs=dtsph_cs, edac=None,
                shift=None
            ) for tol in tols
        }

        self.case_pdata = {
            f'dtsph_edac_tol_{tol:.0e}': dict(
                label=fr'$\epsilon$={tol}', to_plot=True
            )
            for tol in tols
        }

        ########################################
        # Change pseudo time speed of sound
        ########################################
        tol = 1e-4
        # cs = beta*u_max for TG u_max is 1.0
        betas = [2, 5, 10, 20]
        nx = 100
        tf = 2.5

        self.case_info.update({
            f'dtsph_edac_beta_{beta}': dict(
                nx=nx, tol=tol, tf=tf, kernel=kernel, re=re, dtaufac=1/beta,
                perturb=perturb, dtsph_cs=beta, base_command=cmd, edac=None,
                shift=None
            ) for beta in betas
        })

        self.case_pdata.update({
            f'dtsph_edac_beta_{beta}': dict(
                label=fr'DTSPH, $\beta$={beta}', to_plot=True
            )
            for beta in betas
        })

        ########################################
        # Advect pseudo
        ########################################
        nx = 100
        tf = 2.5
        tol = 1e-6
        self.case_info.update({
            'dtsph_edac_advect': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=perturb, base_command=cmd,
                tol=tol, dtsph_advect=None, dtsph_update_nnps=None, edac=None,
                shift=None
            ),
            'dtsph_edac_no_advect': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=perturb, base_command=cmd,
                tol=tol, no_dtsph_advect=None, edac=None, shift=None
            ),
            'dtsph_edac_adv_no_nnps': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=perturb, base_command=cmd,
                tol=tol, dtsph_advect=None, no_dtsph_update_nnps=None, edac=None,
                shift=None
            ),
        })

        self.case_pdata.update({
            f'dtsph_edac_advect': dict(
                label='Advection', to_plot=True
            ),
            f'dtsph_edac_no_advect': dict(
                label='No Advection', to_plot=True
            ),
            f'dtsph_edac_adv_no_nnps': dict(
                label='Advection, No update neighbors', to_plot=True
            ),
            f'dtsph_edac_no_adv_no_nnps': dict(
                label='No Advection, No update neighbors', to_plot=True
            ),
        })

        # XXX: Should we use shifting here.
        ########################################
        # Perturb vs no perturb vs perturb + shifting + only shift
        ########################################
        nx = 100
        tol = 1e-4
        self.case_info.update({
            'dtsph_perturb': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=perturb, base_command=cmd,
                tol=tol, shift=None
            ),
            'dtsph_edac_perturb': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=perturb, base_command=cmd,
                tol=tol, edac=None, shift=None
            ),
            'dtsph_edac_no_perturb': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=0.0, base_command=cmd,
                tol=tol, edac=None, shift=None
            ),
            'dtsph_no_perturb': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=0.0, base_command=cmd,
                tol=tol, shift=None
            )
        })

        self.case_pdata.update({
            'dtsph_perturb': dict(
                label=f'DTSPH, No EDAC, perturb={perturb}', to_plot=False
            ),
            'dtsph_no_perturb': dict(
                label='DTSPH, No EDAC, no perturb', to_plot=False
            ),
            'dtsph_edac_no_perturb': dict(
                label='DTSPH, no perturb', to_plot=True
            ),
            'dtsph_edac_perturb': dict(
                label=f'DTSPH-EDAC, perturb={perturb}', to_plot=True
            ),
        })

        ########################################
        # Different Re and nx
        ########################################
        self.res = res = [100, 200, 500, 1000]
        nxs = [25, 50, 100, 150, 200]
        perturb = 0.1
        self.case_info.update({
            f'dtsph_edac_pert_nx_{nx}_re_{re}': dict(
                nx=nx, re=re, tf=tf, kernel=kernel, perturb=perturb,
                base_command=cmd, edac=None, shift=None, tol=1e-5
            ) for nx, re in product(nxs, res)
        })

        self.case_pdata.update({
            f'dtsph_edac_pert_nx_{nx}_re_{re}': dict(
                label=f'nx={nx}', to_plot=True
            )
            for nx, re in product(nxs, res)
        })

        ########################################
        # Compare case_info
        ########################################
        pysph_cmd = 'pysph run taylor_green' + backend

        dtsph_cs = 10

        _case_info = {
            'wcsph': (
                dict(
                    scheme='wcsph', base_command=pysph_cmd,
                    no_adaptive_timestep=None
                ),
                dict(label='WCSPH', to_plot=True)
            ),
            'deltasph': (
                dict(
                    scheme='wcsph', base_command=pysph_cmd,
                    no_adaptive_timestep=None, delta_sph=None
                ),
                dict(label=r'$\delta$-SPH', to_plot=True)
            ),

            'edac': (
                dict(
                    scheme='edac', base_command=pysph_cmd,
                    no_adaptive_timestep=None
                ),
                dict(label='EDAC', to_plot=True)
            ),
            'tvf': (
                dict(
                    scheme='tvf', base_command=pysph_cmd,
                    no_adaptive_timestep=None
                ),
                dict(label='TVF', to_plot=False)
            ),
            'dtsph': (
                dict(base_command=cmd, tol=1e-4, dtsph_cs=dtsph_cs, shift=None),
                dict(label='DTSPH, No EDAC', to_plot=False)
            ),
            'dtsph_edac': (
                dict(base_command=cmd, tol=1e-4, dtsph_cs=dtsph_cs,
                     edac=None, shift=None),
                dict(label='DTSPH', to_plot=True)
            )
        }

        kernel = 'QuinticSpline'
        perturb = 0.1
        tf = 2.5
        nx = 100
        re = 100
        self.case_info.update({
            f'{s}_pert_scheme': dict(
                re=re, nx=nx, tf=tf, kernel=kernel, perturb=perturb,
                **_case_info[s][0]
            ) for s in _case_info
        })

        self.case_pdata.update({
            f'{s}_pert_scheme': _case_info[s][1]
            for s in _case_info
        })

        self.cases = [
            TGV(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                labels=self.case_pdata[name]['label'],
                cache_nnps=None, **kwargs
            ) for name, kwargs in self.case_info.items()
        ]
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        labels = [
            'tol', 'beta', 'advect', 'perturb', 'scheme', 'nnps'
        ]
        no_pert = 'nopert'

        self._make_tables()
        self._plot_re()
        for label in labels:
            pth = self.output_path(label)
            if not os.path.exists(pth):
                os.mkdir(pth)
            names = [x for x in self.case_info if label in x]

            # Case perturb
            pert_names = [x for x in names if no_pert not in x]
            self._plot_all(labels=[label], names=pert_names, ext='pert_')

            # Case no perturb
            no_pert_names = [x for x in names if no_pert in x]
            if no_pert_names:
                self._plot_all(labels=[label], names=no_pert_names)

        advect_names = [x for x in self.case_info if 'advect' in x]
        cases = filter_by_name(self.cases, advect_names)
        self._plot_particles_advect(cases)

        beta_names = [x for x in self.case_info if 'beta' in x]
        cases = filter_by_name(self.cases, beta_names)
        for i in filter_cases(cases, dtsph_cs=20):
            cases.remove(i)
        # self._plot_particles_3_by_3(cases)

        tol_names = [x for x in self.case_info if 'tol' in x]
        cases = filter_by_name(self.cases, tol_names)
        # self._plot_particles_3_by_3(cases, label='tol')

    def _make_tables(self):
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        cases = [
            ('dtsph_edac_no_advect', 'DTSPH frozen'),
            ('dtsph_edac_adv_no_nnps', 'DTSPH advect, no update neighbors'),
            ('dtsph_edac_advect', 'DTSPH advect, update neighbors'),
        ]
        for name, info in cases:
            if self.case_pdata[name]['to_plot']:
                ci = self.case_info[name]
                row = [info, _get_cpu_time(ci['case'])]
                rows.append(row)
        make_table(
            column_names, rows,
            self.output_path('advect', self.get_name() + '.tex'),
            column_format='lr'
        )

        # comparision with other schemes
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        names = [x for x in self.case_info if x.endswith('scheme')]
        for name in names:
            if self.case_pdata[name]['to_plot']:
                ci = self.case_info[name]
                row = [self.case_pdata[name]['label'],
                       _get_cpu_time(ci['case'])]
                rows.append(row)
        make_table(
            column_names, rows,
            self.output_path('scheme', self.get_name() + '.tex'),
            sort_cols=[1], column_format='lr'
        )

        # changes to beta
        column_names = [r'$\beta$', 'CPU time (secs)']
        rows = []
        names = [x for x in self.case_info if x.startswith('dtsph_beta')]
        for name in names:
            if self.case_pdata[name]['to_plot']:
                ci = self.case_info[name]
                row = [ci['dtsph_cs'], _get_cpu_time(ci['case'])]
                rows.append(row)
        make_table(
            column_names, rows,
            self.output_path('beta', self.get_name() + '.tex'),
            sort_cols=[0], column_format='rr'
        )

        # Changes to tolerance.
        column_names = [r'$\epsilon$', 'CPU time (secs)']
        rows = []
        names = [x for x in self.case_info if '_tol' in x]
        for name in names:
            if self.case_pdata[name]['to_plot']:
                ci = self.case_info[name]
                row = ['%.1g' % ci['tol'], _get_cpu_time(ci['case'])]
                rows.append(row)
        make_table(
            column_names, rows,
            self.output_path('tol', self.get_name() + '.tex'),
            column_format='rr'
        )

    def _plot_re(self):
        import matplotlib.pyplot as plt

        os.mkdir(self.output_path('re'))
        names = [x for x in self.case_info if 're' in x]
        cases = filter_by_name(self.cases, names)

        for re in self.res:
            fig, ax = plt.subplots(1, 2, figsize=(12, 4))
            for case in filter_cases(cases, re=re):
                if not self.case_pdata[case.name]['to_plot']:
                    continue
                re = case.params['re']
                data = np.load(case.input_path('results.npz'))

                t = data['t']
                decay_ex = data['decay_ex']
                ax[0].semilogy(data['t'], data['decay'],
                               label=self.case_pdata[case.name]['label'])
                ax[1].plot(data['t'], data['l1'],
                           label=self.case_pdata[case.name]['label'])
            ax[0].semilogy(t, decay_ex, 'k-', label=fr'Exact $Re={re}$')
            for i in range(2):
                ax[i].legend(loc='best')
                ax[i].set_xlabel('t')
            ax[0].set_ylabel('max velocity')
            ax[1].set_ylabel(r'$L_1$ error')
            fig.savefig(self.output_path('re', f're_{re}.pdf'))
            plt.clf()
            plt.close()

    def _plot_all(self, labels, names, ext=''):
        import matplotlib.pyplot as plt

        cases = filter_by_name(self.cases, names)
        cases = [c for c in cases if self.case_pdata[c.name]['to_plot']]
        size = (5, 4)
        plt.figure(figsize=size)
        compare_runs(cases, 'decay', labels=labels, exact='decay_exact')
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(labels[0], ext + 'decay_all.pdf'))
        plt.close()

        plt.figure(figsize=size)
        compare_runs(cases, 'l1', labels=labels)
        plt.legend(loc='upper left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(labels[0], ext + 'l1_error_all.pdf'))
        plt.close()

    def _plot_particles_advect(self, cases):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files
        from pysph.tools.interpolator import Interpolator

        fig, ax = plt.subplots(1, 2, figsize=(11, 4), sharey=True)
        for i, case in enumerate(cases):
            if self.case_pdata[case.name]['to_plot']:
                files = get_files(case.input_path(), 'taylor_green')
                data = load(files[-1])
                f = data['arrays']['fluid']
                c = np.sqrt(f.u*f.u + f.v*f.v)
                tmp = ax[i].scatter(
                    f.x, f.y, c=c, marker='.', s=8, cmap='viridis',
                    edgecolors='none', vmin=0, vmax=0.12, rasterized=True
                )
                if 'no_dtsph_advect' in case.params:
                    ax[i].set_title('No Advection', fontsize=12)
                else:
                    ax[i].set_title('Advection', fontsize=12)
                ax[i].set_xlabel('x')
        fig.colorbar(tmp, ax=ax.tolist(), shrink=0.95, use_gridspec=True,
                     label='vmag')
        fig.text(0.04, 0.5, 'y', va='center', rotation='vertical')
        plt.savefig(self.output_path('advect', 'comparision_vmag.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

        fig, ax = plt.subplots(1, 2, figsize=plt.figaspect(0.4), sharey=True)
        for i, case in enumerate(cases):
            if self.case_pdata[case.name]['to_plot']:
                if 'no_dtsph_advect' in case.params:
                    files = get_files(case.input_path(), 'taylor_green')
                    data = load(files[-1])
                    f = data['arrays']['fluid']
                    c = np.sqrt(f.u*f.u + f.v*f.v)
                    tmp = ax[1].scatter(
                        f.x, f.y, c=f.p, s=5, cmap='jet',
                        edgecolors='none', rasterized=True
                    )
                    ax[1].set_xlabel('x')
                    ax[1].set_ylabel('y')
                    ax[1].set_xlim(0, 1)
                    ax[1].set_ylim(0, 1)

                    _x = np.linspace(0, 1, 101)
                    xx, yy = np.meshgrid(_x, _x)
                    interp = Interpolator(list(data['arrays'].values()), x=xx,
                                          y=yy)
                    ui = np.zeros_like(xx)
                    vi = np.zeros_like(xx)
                    interp.update_particle_arrays(list(data['arrays'].values()))
                    _u = interp.interpolate('u')
                    _v = interp.interpolate('v')
                    _u.shape = 101, 101
                    _v.shape = 101, 101
                    ui += _u
                    vi += _v
                    ax[0].streamplot(xx, yy, ui, vi, linewidth=0.5, density=2,
                                     color='k')
                    ax[0].set_xlabel('x')
                    ax[0].set_ylabel('y')
        cbaxes = fig.add_axes([0.92, 0.1, 0.02, 0.7])
        fig.colorbar(tmp, ax=ax[1], shrink=0.95, use_gridspec=True,
                     label='p', cax=cbaxes)
        plt.savefig(self.output_path('tg_streamplot.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()

    def _plot_particles_3_by_3(self, cases, label='beta'):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(3, 3, figsize=(8, 6), sharex=True, sharey=True)
        fig.subplots_adjust(wspace=0.2)
        ax = ax.T
        i = 0
        for case in cases:
            if self.case_pdata[case.name]['to_plot']:
                if label == 'beta':
                    param = case.params['dtsph_cs']
                elif case.params['tol']:
                    param = case.params['tol']
                files = get_files(case.input_path(), 'taylor_green')

                times = [0.2, 0.4, 1.0]
                files = get_files_at_given_times(files, times)

                for j, f in enumerate(files):
                    data = load(f)
                    f = data['arrays']['fluid']
                    ax[i, j].scatter(
                        f.x, f.y, marker='.', s=8, c='k', edgecolors='none',
                        alpha=0.8, rasterized=True
                    )
                    ax[i, j].set_xlim([0, 0.6])
                    ax[i, j].set_ylim([0.4, 1.0])
                    if j == 2:
                        if label == 'beta':
                            ax[i, j].set_xlabel(r'$\beta = %d$' % param,
                                                fontsize=12)
                        if label == 'tol':
                            ax[i, j].set_xlabel(r'$\epsilon = %.0e$' % param,
                                                fontsize=12)
                ax[0, i].set_ylabel(r'$t = %.1f$' % times[i], fontsize=12)
                i += 1
        plt.tight_layout(pad=0)
        fig.savefig(self.output_path(label, 'comparision_vmag.pdf'))
        plt.close()

    def _plot_streamlines(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(2, 3, figsize=(8, 6), sharex=True, sharey=True)
        ax = ax.ravel()
        fig.subplots_adjust(wspace=0.2)
        i = 0
        for case in self.cases:
            files = get_files(case.input_path(), 'cavity')
            data = load(files[-1])
            f = data['arrays']['fluid']
            t = data['solver_data']['t']
            ax[i].streamplots(
                f.x, f.y, marker='.', s=8, c='k', edgecolors='none',
                alpha=0.8, rasterized=True
            )
            ax[i].set_xlim([0, 1])
            ax[i].set_ylim([0, 1])
            i += 1
        plt.tight_layout(pad=0)
        fig.savefig(self.output_path('comparision_vmag.pdf'))
        plt.close()


def perf_plot(out_dir):
    import matplotlib.pyplot as plt

    path = os.path.join(out_dir, 'performance.json')
    if os.path.exists(path):
        with open(path, 'r') as f:
            data = json.load(f)
    else:
        return
    perf_dir = os.path.join(out_dir, 'performance')
    shutil.rmtree(perf_dir)
    os.mkdir(perf_dir)

    problems = {
        # 'cavity': dict(prob='Lid-driven-cavity'),
        'dam_break_2d': dict(prob='Dam-break 2d'),
        'dam_break_3d': dict(prob='Dam-break 3d')
    }
    for key in problems:
        if key not in data.keys():
            problems.pop(key)

    to_plot = {}
    for name in problems:
        all_times, all_labels = [], []
        for v in data[name].values():
            if v['measure_perf']:
                all_times.append(v['time'])
                all_labels.append(v['label'])
        labels_wo_dtsph = all_labels[:]
        times_wo_dtsph = all_times[:]
        dtsph_times, xticks = [], []
        for label, time in zip(all_labels, all_times):
            if 'DTSPH' in label:
                dtsph_times.append(time)
                xticks.append(label)
                idx = labels_wo_dtsph.index(label)
                labels_wo_dtsph.pop(idx)
                times_wo_dtsph.pop(idx)
        for xtick, time in zip(xticks, dtsph_times):
            speedup = [t/time for t in times_wo_dtsph]
            labels = labels_wo_dtsph[:]
            to_plot[xtick] = dict(
                labels=labels, speedup=speedup,
                xticks=xtick, prob=problems[name]['prob']
            )

    bar_width = 4
    xtick, xtick_name = [], []
    plt.figure(figsize=(6, 5))
    for i, n in enumerate(to_plot):
        speedup = to_plot[n]['speedup']
        w = np.arange(len(speedup), dtype=np.float64)
        w += i*bar_width
        rects = plt.bar(w, speedup, alpha=0.5, align='center', width=0.9)
        for j, r in enumerate(rects):
            height = r.get_height()
            plt.text(r.get_x() + r.get_width()/2.0, 1.05*height,
                     to_plot[n]['labels'][j], size=6, ha='center')
        xtick.append((w[0] + w[-1])/2)
        xtick_name.append(to_plot[n]['prob'] + ',\n' + to_plot[n]['xticks'])
    plt.xticks(xtick, xtick_name)
    plt.ylim([0, 20])
    plt.ylabel(r'Speed up')
    plt.savefig(os.path.join(perf_dir, 'perf.pdf'))
    plt.close()


if __name__ == '__main__':
    problems = [
        SquarePatch,
        EllipticalDrop,
        DamBreak2D,
        DamBreak3D,
        Cavity,
        SteadyCavity,
        TaylorGreen,
    ]

    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=problems
    )
    automator.run()
    perf_plot(automator.output_dir)
