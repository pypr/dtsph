import numpy as np
import pysph.examples.lattice_cylinders as LC
from dtsph import DTSPHScheme


class LatticeCylindersDTSPH(LC.LatticeCylinders):
    def configure_scheme(self):
        self.scheme.configure_solver(
            dt=10*LC.dt, tf=LC.tf, adaptive_timestep=False, pfreq=10,
        )

    def create_scheme(self):
        dtsph = DTSPHScheme(
            ['fluid'], ['solid'], dim=2, nu=LC.nu, dtaufac=0.2, cs=LC.c0,
            vref=LC.Umax, rho=LC.rho0, debug=False, has_ghosts=True, gx=LC.fx
        )
        return dtsph

    def create_particles(self):
        [fluid, solid] = super(LatticeCylindersDTSPH, self).create_particles()

        fluid.un_p[:] = 0.0
        fluid.vn_p[:] = 0.0

        nfp = fluid.get_number_of_particles()
        fluid.orig_idx[:] = np.arange(nfp)
        return [fluid, solid]


if __name__ == '__main__':
    app = LatticeCylindersDTSPH()
    app.run()
    app.post_process(app.info_filename)
