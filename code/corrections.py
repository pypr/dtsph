from compyle.api import declare
from pysph.sph.equation import Equation, Group, BasicCodeBlock
from pysph.sph.wc.linalg import gj_solve, augmented_matrix, identity
from mako.template import Template


class GradientCorrectionPreStep(Equation):
    """`L` is a symmetric matrix so we only need to
    store `dim*(dim + 1)/2` values. First we build `L` matrix,
    then compute the inverse for every particle. We use `Linv`
    property throughout.
    """
    def __init__(self, dest, sources, dim):
        # Add `1` for the zeroth moment.
        self.sim_dim = dim + 1
        super().__init__(dest, sources)

    def initialize(self, d_Linv, d_idx, d_L):
        i, j, dim, dim2 = declare('int', 4)
        dim = 4
        dim2 = 16

        for i in range(dim):
            for j in range(dim):
                d_L[d_idx*dim2 + i*dim + j] = 0.0
                d_Linv[d_idx*dim2 + i*dim + j] = 0.0
                if i == j:
                    d_Linv[d_idx*dim2 + i*dim + j] = 1.0

    def _get_helpers_(self):
        return [gj_solve, augmented_matrix, identity]

    def loop(self, d_L, d_idx, s_m, s_rho, s_idx, d_h, RIJ, XIJ,
            SPH_KERNEL):
        V_j = s_m[s_idx] / s_rho[s_idx]

        idx, i, j, dim, dim2 = declare('int', 5)
        dim = 4
        dim2 = 16
        idx = dim2*d_idx

        # Append `1` to XIJ to compute zeroth moment.
        YJI = declare('matrix(4)')
        YJI[0] = 1.0
        YJI[1] = -XIJ[0]
        YJI[2] = -XIJ[1]
        YJI[3] = -XIJ[2]

        dwi = declare('matrix(3)')
        HI = d_h[d_idx]
        SPH_KERNEL.gradient(XIJ, RIJ, HI, dwi)

        wi = SPH_KERNEL.kernel(XIJ, RIJ, HI)
        DIJ = declare('matrix(4)')
        DIJ[0] = wi
        DIJ[1] = dwi[0]
        DIJ[2] = dwi[1]
        DIJ[3] = dwi[2]

        for i in range(dim):
            for j in range(dim):
                d_L[idx + dim*i + j] += V_j*DIJ[i]*YJI[j]

    def post_loop(self, d_Linv, d_idx, d_L):
        i, j, idx, dim, dim2 = declare('int', 6)
        dim = 4
        dim2 = 16

        tmp = declare('matrix(16)')
        idn = declare('matrix(16)')

        identity(tmp, dim)
        identity(idn, dim)

        idx = dim2*d_idx

        for i in range(self.sim_dim):
            for j in range(self.sim_dim):
                tmp[dim*i + j] = d_L[idx + i*dim + j]

        aug_matrix = declare('matrix(32)')
        augmented_matrix(tmp, idn, dim, dim, dim, aug_matrix)

        error_code = gj_solve(aug_matrix, dim, dim, tmp)
        # If singular make d_Linv an identity matrix.
        if abs(error_code) < 0.5:
            for i in range(dim):
                for j in range(dim):
                    d_Linv[idx + dim*i + j] = tmp[dim*i + j]


class KGFCorrectionPreStep(GradientCorrectionPreStep):
    def loop(self, d_L, d_idx, s_m, s_rho, s_idx, d_h, RIJ, XIJ,
            SPH_KERNEL):
        V_j = s_m[s_idx] / s_rho[s_idx]

        idx, i, j, dim, dim2 = declare('int', 5)
        dim = 4
        dim2 = 16
        idx = dim2*d_idx

        # Append `1` to XIJ to compute zeroth moment.
        YJI = declare('matrix(4)')
        YJI[0] = 1.0
        YJI[1] = -XIJ[0]
        YJI[2] = -XIJ[1]
        YJI[3] = -XIJ[2]

        HI = d_h[d_idx]
        wi = SPH_KERNEL.kernel(XIJ, RIJ, HI)

        for i in range(dim):
            for j in range(dim):
                d_L[idx + dim*i + j] += V_j*wi*YJI[i]*YJI[j]


class GradientCorrection(Equation):
    def loop(self, d_idx, d_Linv, WI, DWI):
        Linv = declare('matrix(16)')
        i = declare('int')
        for i in range(16):
            Linv[i] = d_Linv[d_idx*16 + i]

        WI = Linv[0]*WI + Linv[1]*DWI[0] + Linv[2]*DWI[1] + Linv[3]*DWI[2]

        DWI[0] = Linv[4]*WI + Linv[5]*DWI[0] + Linv[6]*DWI[1] + Linv[7]*DWI[2]
        DWI[1] = Linv[8]*WI + Linv[9]*DWI[0] + Linv[10]*DWI[1] + Linv[11]*DWI[2]
        DWI[2] = (Linv[12]*WI + Linv[13]*DWI[0] +
                  Linv[14]*DWI[1] + Linv[15]*DWI[2])


class KGFCorrection(Equation):
    def loop(self, d_idx, d_Linv, WI, DWI, XIJ):
        Linv = declare('matrix(16)')
        i = declare('int')
        for i in range(16):
            Linv[i] = d_Linv[d_idx*16 + i]

        WI = WI*(Linv[0] - Linv[1]*XIJ[0] - Linv[2]*XIJ[1] - Linv[3]*XIJ[2])

        DWI[0] = WI*(Linv[4] - Linv[5]*XIJ[0] -
                     Linv[6]*XIJ[1] - Linv[7]*XIJ[2])
        DWI[1] = WI*(Linv[8] - Linv[9]*XIJ[0] -
                     Linv[10]*XIJ[1] - Linv[11]*XIJ[2])
        DWI[2] = WI*(Linv[12] - Linv[13]*XIJ[0] -
                  Linv[14]*XIJ[1] - Linv[15]*XIJ[2])


def grad_correction_precomp():
    dim = 3
    dim1 = dim + 1
    dim2 = dim1*dim1

    Linv_code = Template("""
        %for i in range(dim2):
        Linv[${i}] = d_Linv[d_idx*${dim2} + ${i}]
        %endfor
    """).render(dim2=dim2)

    HI_code = Template("""
        HI = d_h[d_idx]
    """).render()

    OLD_WI_code = Template("""
        OLD_WI = KERNEL(XIJ, RIJ, HI)
    """).render()

    ODWI_code = Template("""
        GRADIENT(XIJ, RIJ, HI, ODWI)
    """).render()

    WI_code = Template("""
        WI = Linv[0]*OLD_WI + Linv[1]*ODWI[0] + Linv[2]*ODWI[1] + Linv[3]*ODWI[2]
    """).render()

    DWI_code = Template("""
        DWI[0] = Linv[4]*OLD_WI + Linv[5]*ODWI[0] + Linv[6]*ODWI[1] + Linv[7]*ODWI[2]
        DWI[1] = Linv[8]*OLD_WI + Linv[9]*ODWI[0] + Linv[10]*ODWI[1] + Linv[11]*ODWI[2]
        DWI[2] = Linv[12]*OLD_WI + Linv[13]*ODWI[0] + Linv[14]*ODWI[1] + Linv[15]*ODWI[2]
    """).render()

    Group.pre_comp.update(
        Linv=BasicCodeBlock(
            code=Linv_code,
            Linv=[1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        ),
        OLD_WI=BasicCodeBlock(
            code=OLD_WI_code,
            OLD_WI=0.0
        ),
        WI=BasicCodeBlock(
            code=WI_code,
            WI=0.0
        ),
        DWI=BasicCodeBlock(
            code=DWI_code,
            DWI=[0.0, 0.0, 0.0]
        ),
        ODWI=BasicCodeBlock(
            code=ODWI_code,
            ODWI=[0.0, 0.0, 0.0]
        ),
        HI=BasicCodeBlock(
            code=HI_code,
            HI=0.0
        )
    )


def kgf_precomp():
    dim = 3
    dim1 = dim + 1
    dim2 = dim1*dim1

    Linv_code = Template("""
        %for i in range(dim2):
        Linv[${i}] = d_Linv[d_idx*${dim2} + ${i}]
        %endfor
    """).render(dim2=dim2)

    HI_code = Template("""
        HI = d_h[d_idx]
    """).render()

    OLD_WI_code = Template("""
        OLD_WI = KERNEL(XIJ, RIJ, HI)
    """).render()

    WI_code = Template("""
        WI = Linv[0]*OLD_WI - Linv[1]*OLD_WI*XIJ[0] - Linv[2]*OLD_WI*XIJ[1] - Linv[3]*OLD_WI*XIJ[2]
    """).render()

    DWI_code = Template("""
        DWI[0] = Linv[4]*OLD_WI - Linv[5]*OLD_WI*XIJ[0] - Linv[6]*OLD_WI*XIJ[1] - Linv[7]*OLD_WI*XIJ[2]
        DWI[1] = Linv[8]*OLD_WI - Linv[9]*OLD_WI*XIJ[0] - Linv[10]*OLD_WI*XIJ[1] - Linv[11]*OLD_WI*XIJ[2]
        DWI[2] = Linv[12]*OLD_WI - Linv[13]*OLD_WI*XIJ[0] - Linv[14]*OLD_WI*XIJ[1] - Linv[15]*OLD_WI*XIJ[2]
    """).render()

    Group.pre_comp.update(
        Linv=BasicCodeBlock(
            code=Linv_code,
            Linv=[1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        ),
        OLD_WI=BasicCodeBlock(
            code=OLD_WI_code,
            OLD_WI=0.0
        ),
        WI=BasicCodeBlock(
            code=WI_code,
            WI=0.0
        ),
        DWI=BasicCodeBlock(
            code=DWI_code,
            DWI=[0.0, 0.0, 0.0]
        ),
        HI=BasicCodeBlock(
            code=HI_code,
            HI=0.0
        )
    )


def reset_precomp():
    keys = ['Linv', 'OLD_WI', 'WI', 'DWI', 'HI', 'ODWI']
    for k in keys:
        Group.pre_comp.pop(k)

    Group.pre_comp.update(
        DWI=BasicCodeBlock(
            code="GRADIENT(XIJ, RIJ, d_h[d_idx], DWI)",
            DWI=[0.0, 0.0, 0.0]
        ),
        WI=BasicCodeBlock(
            code="WI = KERNEL(XIJ, RIJ, d_h[d_idx])",
            WI=0.0
        )
    )


class Debug(Equation):
    def py_initialize(self, dst, t, dt):
        import pudb; pudb.set_trace()
        t = dst.Linv
